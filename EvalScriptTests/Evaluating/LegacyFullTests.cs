﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Evaluating
{
    public class LegacyFullTests
    {
        [Fact]
        public void StringLiteralLength()
        {
            var code = "'hello'.Length";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => null);
            Assert.Equal(5, output);
        }

        [Fact]
        public void NegativeFilteredArrayDefCount()
        {
            var code = "-[1, 2, 3, 4, 5].Count(x => x >= 3)";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => null);
            Assert.Equal(-3, output);
        }

        [Fact]
        public void NotAnyInArray()
        {
            var code = "!Any(items, x => x == 10)";
            var obj = new List<int>() { 1, 2, 3, 4 };
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => obj);
            Assert.Equal(true, output);
        }

        [Fact]
        public void TrimString()
        {
            var code = "'  hi  '.Trim()";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => null);
            Assert.Equal("hi", output);
        }

        [Fact]
        public void TrimToUpperString()
        {
            var code = "'  hi  '.Trim().ToUpper()";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => null);
            Assert.Equal("HI", output);
        }

        [Fact]
        public void DictionaryProperty()
        {
            var code = "[FirstName:'James', LastName:'Coyle'].LastName";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.LegacyEvaluator().Run(input, s => null);
            Assert.Equal("Coyle", output);
        }

        [Fact]
        public void FormatTest()
        {
            var code = "1.0 / 3 #0.0";
            var input = new EvalScript.Interpreting.Interpreter().Run(code);
            var output = new EvalScript.Evaluating.StandardEvaluator().Run(input, s => null);
            Assert.Equal("0.3", output);
        }
    }
}
