﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class KeyValuePairBuilderTests
    {
        [Fact]
        public void LiteralPair()
        {
            var input = new List<Token>()
            {
                new PropertyToken("a"),
                new Token(Stage2Types.KeyValueOperator),
                new LiteralToken(1)
            };
            new KeyValuePairBuilder().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            var output = input[0] as KeyValuePairToken;
            Assert.Equal("a", output.Key);
            Assert.Equal(1, output.ValueToken.Value);
        }
    }
}
