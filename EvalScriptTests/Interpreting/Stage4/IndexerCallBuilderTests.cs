﻿using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class IndexerCallBuilderTests
    {
        [Fact]
        public void PropertyIndexer()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("myList[1]");
            var chain = output as ChainToken;
            Assert.Equal(2, chain.Items.Count());
            var property = chain.Items.First() as PropertyToken;
            Assert.Equal("myList", property.Name);
            var indexer = chain.Items.Last() as IndexerCallToken;
            var indexerParam = indexer.Parameter as LiteralToken;
            Assert.Equal(1, indexerParam.Value);
        }

        [Fact]
        public void FunctionIndexer()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("GetPerson()['First Name']");
            var chain = output as ChainToken;
            Assert.Equal(2, chain.Items.Count());
            var function = chain.Items.First() as FunctionCallToken;
            Assert.Equal("GetPerson", function.Name);
            Assert.Empty(function.Parameters);
            var indexer = chain.Items.Last() as IndexerCallToken;
            var indexerParam = indexer.Parameter as LiteralToken;
            Assert.Equal("First Name", indexerParam.Value);
        }

        [Fact]
        public void ArrayIndexer()
        {
            var output = new EvalScript.Interpreting.Interpreter().Run("[1, 2, 3, 4][1]");
            var chain = output as ChainToken;
            Assert.Equal(2, chain.Items.Count());
            var arrayDef = chain.Items.First() as ArrayDefinitionToken;
            Assert.Equal(4, arrayDef.Items.Count());
            var indexer = chain.Items.Last() as IndexerCallToken;
            var indexerParam = indexer.Parameter as LiteralToken;
            Assert.Equal(1, indexerParam.Value);
        }
    }
}
