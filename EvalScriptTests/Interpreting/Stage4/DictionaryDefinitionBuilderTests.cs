﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage3;
using EvalScript.Interpreting.Stage4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage4
{
    public class DictionaryDefinitionBuilderTests
    {
        [Fact]
        public void SimpleDef()
        {
            var input = new List<Token>()
            {
                new ParentToken(Stage3Types.SquareBrackets,
                    new KeyValuePairToken("a", new LiteralToken(1)),
                    new Token(Stage2Types.Comma),
                    new KeyValuePairToken("b", new LiteralToken(2)),
                    new Token(Stage2Types.Comma),
                    new KeyValuePairToken("c", new LiteralToken(3))
                )
            };
            new DictionaryDefinitionBuilder().Run(new EvalScript.Interpreting.Interpreter(), input);
            var output = input[0] as DictionaryDefinitionToken;
            Assert.Equal(3, output.Items.Count());
            Assert.Equal("a", (output.Items.ToList()[0] as KeyValuePairToken).Key);
            Assert.Equal(1, (output.Items.ToList()[0] as KeyValuePairToken).ValueToken.Value);
            Assert.Equal("b", (output.Items.ToList()[1] as KeyValuePairToken).Key);
            Assert.Equal(2, (output.Items.ToList()[1] as KeyValuePairToken).ValueToken.Value);
            Assert.Equal("c", (output.Items.ToList()[2] as KeyValuePairToken).Key);
            Assert.Equal(3, (output.Items.ToList()[2] as KeyValuePairToken).ValueToken.Value);
        }
    }
}
