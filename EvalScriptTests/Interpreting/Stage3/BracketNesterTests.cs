﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage2;
using EvalScript.Interpreting.Stage3;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage3
{
    public class BracketNesterTests
    {
        [Fact]
        public void Brackets()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.PropertyName, "scaling"),
                new Token(Stage2Types.MultiplyOperator),
                new Token(Stage2Types.LeftBracket),
                new Token(Stage2Types.NumericLiteral, 2),
                new Token(Stage2Types.PlusOperator),
                new Token(Stage2Types.NumericLiteral, 3),
                new Token(Stage2Types.RightBracket)
            };
            new BracketNester(BracketNester.Types.Brackets).Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, input.Count);
            Assert.Equal(Stage3Types.Brackets, input[2].Type);
            var brackets = input[2] as ParentToken;
            Assert.Equal(3, brackets.Children.Count);
        }

        [Fact]
        public void SquareBrackets()
        {
            var input = new List<Token>()
            {
                new Token(Stage2Types.PropertyName, "scaling"),
                new Token(Stage2Types.LeftSquareBracket),
                new Token(Stage2Types.NumericLiteral, 2),
                new Token(Stage2Types.PlusOperator),
                new Token(Stage2Types.NumericLiteral, 3),
                new Token(Stage2Types.RightSquareBracket)
            };
            new BracketNester(BracketNester.Types.SquareBrackets).Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal(Stage3Types.SquareBrackets, input[1].Type);
            var brackets = input[1] as ParentToken;
            Assert.Equal(3, brackets.Children.Count);
        }
    }
}
