﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage1
{
    public class FormattingIdentifierTests
    {
        [Fact]
        public void SimpleFormat()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "100 # 0.##")
            };
            var format = new FormattingIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal("100 ", input[0].Value);
            Assert.Equal("0.##", format);
        }

        [Fact]
        public void FormatWithStringLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.StringLiteral, "Raffle #"),
                new Token(Stage1Types.Text, ".Length # 0.00")
            };
            var format = new FormattingIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, input.Count);
            Assert.Equal(".Length ", input[1].Value);
            Assert.Equal("0.00", format);
        }
    }
}
