﻿using EvalScript;
using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage1
{
    public class StringLiteralSeparatorTests
    {
        [Fact]
        public void StringLiteralAtEnd()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "This is a 'test'")
            };
            var output = new StringLiteralSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, output.Count);
            Assert.Equal(Stage1Types.StringLiteral, output[1].Type);
            Assert.Equal("test", output[1].Value);
        }

        [Fact]
        public void StringLiteralAtStart()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "'This' is a test")
            };
            var output = new StringLiteralSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, output.Count);
            Assert.Equal(Stage1Types.StringLiteral, output[0].Type);
            Assert.Equal("This", output[0].Value);
        }

        [Fact]
        public void StringLiteralInMiddle()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "This 'is a' test")
            };
            var output = new StringLiteralSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, output.Count);
            Assert.Equal(Stage1Types.StringLiteral, output[1].Type);
            Assert.Equal("is a", output[1].Value);
        }

        [Fact]
        public void EscapedStringLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "'\\'This\\'' is a test")
            };
            var output = new StringLiteralSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(2, output.Count);
            Assert.Equal(Stage1Types.StringLiteral, output[0].Type);
            Assert.Equal("'This'", output[0].Value);
        }

        [Fact]
        public void UnclosedStringLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "Hello 'world")
            };
            Assert.Throws<SyntaxException>(() => new StringLiteralSeparator().Run(new EvalScript.Interpreting.Interpreter(), input));
        }
    }
}
