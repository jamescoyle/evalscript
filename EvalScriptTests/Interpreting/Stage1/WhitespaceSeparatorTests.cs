﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage1
{
    public class WhitespaceSeparatorTests
    {
        [Fact]
        public void SingleSpaces()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "test test")
            };
            var output = new WhitespaceSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, output.Count);
            Assert.Equal(Stage1Types.Whitespace, output[1].Type);
        }

        [Fact]
        public void MultipleSpaces()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "test   test")
            };
            var output = new WhitespaceSeparator().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Equal(3, output.Count);
            Assert.Equal(Stage1Types.Whitespace, output[1].Type);
        }
    }
}
