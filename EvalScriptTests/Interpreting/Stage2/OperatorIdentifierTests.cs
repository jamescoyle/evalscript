﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class OperatorIdentifierTests
    {
        [Fact]
        public void LessThan()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.LeftAngleBracket)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.LessThanOperator, input[0].Type);
        }

        [Fact]
        public void LessThanOrEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.LeftAngleBracket),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.LessThanOrEqualOperator, input[0].Type);
        }

        [Fact]
        public void LT()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "LT")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.LessThanOperator, input[0].Type);
        }

        [Fact]
        public void LTEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "LT"),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.LessThanOrEqualOperator, input[0].Type);
        }

        [Fact]
        public void GreaterThan()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.RightAngleBracket)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.GreaterThanOperator, input[0].Type);
        }

        [Fact]
        public void GreaterThanOrEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.RightAngleBracket),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.GreaterThanOrEqualOperator, input[0].Type);
        }

        [Fact]
        public void GT()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "GT")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.GreaterThanOperator, input[0].Type);
        }

        [Fact]
        public void GTEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "GT"),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.GreaterThanOrEqualOperator, input[0].Type);
        }

        [Fact]
        public void Equal1()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.EqualOperator, input[0].Type);
        }

        [Fact]
        public void Equal2()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Equal),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.EqualOperator, input[0].Type);
        }

        [Fact]
        public void Not()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.ExclamationMark)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.NotOperator, input[0].Type);
        }

        [Fact]
        public void NotEqual()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.ExclamationMark),
                new Token(Stage1Types.Equal)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.NotEqualOperator, input[0].Type);
        }

        [Fact]
        public void Plus()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Plus)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.PlusOperator, input[0].Type);
        }

        [Fact]
        public void Minus()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Hyphen)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.MinusOperator, input[0].Type);
        }

        [Fact]
        public void Multiply()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Asterisk)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.MultiplyOperator, input[0].Type);
        }

        [Fact]
        public void Divide()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.ForwardSlash)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.DivideOperator, input[0].Type);
        }

        [Fact]
        public void Power()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Caret)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.PowerOperator, input[0].Type);
        }

        [Fact]
        public void NullCoalesce()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.QuestionMark),
                new Token(Stage1Types.QuestionMark)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.NullCoalesceOperator, input[0].Type);
        }

        [Fact]
        public void Modulus()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Percent)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.ModulusOperator, input[0].Type);
        }

        [Fact]
        public void As1()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "as")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.AsOperator, input[0].Type);
        }

        [Fact]
        public void As2()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "AS")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.AsOperator, input[0].Type);
        }

        [Fact]
        public void And1()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "and")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.AndOperator, input[0].Type);
        }

        [Fact]
        public void And2()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "AND")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.AndOperator, input[0].Type);
        }

        [Fact]
        public void And3()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Ampersand),
                new Token(Stage1Types.Ampersand)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.AndOperator, input[0].Type);
        }

        [Fact]
        public void Or1()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "or")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.OrOperator, input[0].Type);
        }

        [Fact]
        public void Or2()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "OR")
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.OrOperator, input[0].Type);
        }

        [Fact]
        public void Or3()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Pipe),
                new Token(Stage1Types.Pipe)
            };
            new OperatorIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.OrOperator, input[0].Type);
        }
    }
}
