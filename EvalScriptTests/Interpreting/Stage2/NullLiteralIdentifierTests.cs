﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class NullLiteralIdentifierTests
    {
        [Fact]
        public void NullLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "null")
            };
            new NullLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage2Types.NullLiteral, input[0].Type);
            Assert.Null(input[0].Value);
        }

        [Fact]
        public void NotNullLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "noll")
            };
            new NullLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage1Types.Text, input[0].Type);
            Assert.Equal("noll", input[0].Value);
        }
    }
}
