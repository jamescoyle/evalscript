﻿using EvalScript.Interpreting;
using EvalScript.Interpreting.Stage1;
using EvalScript.Interpreting.Stage2;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace EvalScriptTests.Interpreting.Stage2
{
    public class BooleanLiteralIdentifierTests
    {
        [Fact]
        public void TrueLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "true")
            };
            new BooleanLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.True((bool)input[0].Value);
        }

        [Fact]
        public void FalseLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "false")
            };
            new BooleanLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.False((bool)input[0].Value);
        }

        [Fact]
        public void NonBooleanLiteral()
        {
            var input = new List<Token>()
            {
                new Token(Stage1Types.Text, "fase")
            };
            new BooleanLiteralIdentifier().Run(new EvalScript.Interpreting.Interpreter(), input);
            Assert.Single(input);
            Assert.Equal(Stage1Types.Text, input[0].Type);
            Assert.Equal("fase", input[0].Value);
        }
    }
}
